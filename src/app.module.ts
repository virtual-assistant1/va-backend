import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { GraphQLModule } from '@nestjs/graphql';

import { UserModule } from './users/user.module';
import { MlModule } from './ml/ml.module';
import { SharedModule } from './shared/shared.module';
import { ConfigModule } from './config/config.module';
import { TreatmentModule } from './treatment/treatment.module';

import { ConfigService } from './config/config.service';
import { DoctorsModule } from './doctors/doctors.module';
import { AppointmentsModule } from './appointments/appointments.module';

const context = ({ req, res }) => ({ req, res });

const MongoDB = TypegooseModule.forRootAsync({
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => ({
    uri: configService.databaseURL,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }),
});

const GraphQL = GraphQLModule.forRootAsync({
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => ({
    autoSchemaFile: configService.graphqlSchema,
    path: configService.graphqlPath,
    context,
    playground: true,
    debug: true,
    introspection: true,
  }),
});

@Module({
  imports: [
    MongoDB,
    GraphQL,
    SharedModule,
    UserModule,
    MlModule,
    TreatmentModule,
    DoctorsModule,
    AppointmentsModule,
  ],
})
export class AppModule {}
