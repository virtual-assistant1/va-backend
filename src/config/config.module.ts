import { Module } from '@nestjs/common';
import { ConfigService } from './config.service';

export const config = new ConfigService(
  `.env.${process.env.NODE_ENV || 'development'}`,
);

@Module({
  providers: [
    {
      provide: ConfigService,
      useValue: config,
    },
  ],
  exports: [ConfigService],
})
export class ConfigModule {}
