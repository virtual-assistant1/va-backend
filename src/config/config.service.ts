import * as dotenv from 'dotenv';
import * as Joi from '@hapi/joi';
import * as fs from 'fs';

export type EnvConfig = Record<string, string>;

export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor(filePath: string) {
    const config = dotenv.parse(fs.readFileSync(filePath));
    this.envConfig = this.validateInput(config);
  }

  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      DB_URL: Joi.string().required(),
      SECRET: Joi.string().required(),
      IS_PROD: Joi.boolean().required(),
      GRAPHQL_PATH: Joi.string().required(),
      GRAPHQL_SCHEMA: Joi.string().required(),
      ACCESS_TOKEN_EXPIRY: Joi.string().required(),
      REFRESH_TOKEN_EXPIRY: Joi.string().required(),
      ML_URL: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = envVarsSchema.validate(
      envConfig,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }

  get(key: string): string {
    return this.envConfig[key];
  }

  get isProd(): boolean {
    return Boolean(this.envConfig.IS_PROD);
  }

  get secret(): string {
    return String(this.envConfig.SECRET);
  }

  get databaseURL(): string {
    return String(this.envConfig.DB_URL);
  }

  get graphqlSchema(): string {
    return String(this.envConfig.GRAPHQL_SCHEMA);
  }
  get graphqlPath(): string {
    return String(this.envConfig.GRAPHQL_PATH);
  }

  get accessTokenExpiry(): string {
    return String(this.envConfig.ACCESS_TOKEN_EXPIRY);
  }

  get refreshTokenExpiry(): string {
    return String(this.envConfig.REFRESH_TOKEN_EXPIRY);
  }

  get mlURL(): string {
    return String(this.envConfig.ML_URL);
  }
}
