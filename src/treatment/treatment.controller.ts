import { Controller, Get, UseGuards, Param } from '@nestjs/common';
import { TreatmentService } from './treatment.service';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';

import { AuthGuard } from '../auth/auth.guard';
@ApiBearerAuth()
@ApiTags('Diseases Treatments APIs')
@Controller('treatment')
export class TreatmentController {
  constructor(private readonly treatmentService: TreatmentService) {}
  @UseGuards(AuthGuard)
  @Get('/:diseaseCode')
  @ApiOperation({
    summary:
      'API to retrieve the disease up to date treatments scientific articles',
  })
  index(@Param('diseaseCode') diseaseCode: number) {
    return this.treatmentService.getDiseaseTreatments(diseaseCode);
  }
}
