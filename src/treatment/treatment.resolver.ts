import { Resolver, Query, Args } from '@nestjs/graphql';
import { TreatmentService } from './treatment.service';
import { AuthGuard } from '../auth/auth.guard';
import { UseGuards } from '@nestjs/common';
import { Treatment } from './treatment.model';

@Resolver(() => Treatment)
export class TreatmentResolver {
  constructor(private treatmentService: TreatmentService) {}

  @Query(() => [Treatment])
  @UseGuards(AuthGuard)
  diseaseTreatments(@Args('diseaseCode') diseaseCode: number) {
    return this.treatmentService.getDiseaseTreatments(diseaseCode);
  }
}
