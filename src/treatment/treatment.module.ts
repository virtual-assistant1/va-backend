import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { Treatment } from './treatment.model';
import { TreatmentService } from './treatment.service';
import { TreatmentController } from './treatment.controller';
import { TreatmentResolver } from './treatment.resolver';

@Module({
  imports: [
    TypegooseModule.forFeature([
      {
        typegooseClass: Treatment,
        schemaOptions: {
          timestamps: true,
        },
      },
    ]),
  ],
  controllers: [TreatmentController],
  providers: [TreatmentService, TreatmentResolver],
})
export class TreatmentModule {}
