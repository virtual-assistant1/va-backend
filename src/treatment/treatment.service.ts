import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Treatment } from './treatment.model';

@Injectable()
export class TreatmentService {
  constructor(
    @InjectModel(Treatment)
    private readonly treatmentRepo: ReturnModelType<typeof Treatment>,
  ) {}

  getDiseaseTreatments(diseaseCode: number) {
    return this.treatmentRepo.find({ diseaseCode });
  }
}
