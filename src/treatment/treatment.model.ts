import { prop } from '@typegoose/typegoose';
import { ObjectType, ID, Field } from '@nestjs/graphql';

@ObjectType()
export class Treatment {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  title: string;

  @Field()
  @prop()
  summary: string;

  @Field()
  @prop()
  url: string;

  @Field()
  @prop()
  diseaseCode: number;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}
