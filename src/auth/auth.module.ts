import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { AuthResolver } from './auth.resolver';
import { UserModule } from '../users/user.module';
import { AuthController } from './auth.controller';
import { config } from '../config/config.module';

@Module({
  imports: [
    UserModule,
    JwtModule.register({
      secret: config.secret,
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, AuthResolver],
  exports: [AuthService],
})
export class AuthModule {}
