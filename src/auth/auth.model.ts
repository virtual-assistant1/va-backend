import { ObjectType, Field } from '@nestjs/graphql';
import { User } from '../users/user.model';

@ObjectType()
export class Auth {
  @Field(() => User)
  user: User;

  @Field()
  accessToken: string;

  @Field()
  refreshToken: string;
}
