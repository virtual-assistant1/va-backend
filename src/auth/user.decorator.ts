import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const CurrentUser = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);

    if (!ctx.getContext().req) {
      return context.switchToHttp().getRequest().user;
    }
    return ctx.getContext().req.user;
  },
);
