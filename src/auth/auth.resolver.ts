import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { LoginDto, RefreshTokensDto } from './dto';
import { Auth } from './auth.model';

@Resolver(() => Auth)
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation(() => Auth)
  login(@Args() args: LoginDto) {
    return this.authService.validateUser(args.email, args.password);
  }

  @Mutation(() => Auth)
  refreshTokens(@Args() args: RefreshTokensDto) {
    return this.authService.refreshTokens(args.accessToken, args.refreshToken);
  }
}
