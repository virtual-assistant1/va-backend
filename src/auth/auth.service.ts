import * as bcrypt from 'bcrypt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../users/user.service';
import { Auth } from './auth.model';
import { Request } from 'express';
import { ConfigService } from '../config/config.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async validateRequest(request: any): Promise<boolean> {
    const accessToken = this.extractAccessToken(request);
    let user = null;
    try {
      user = await this.jwtService.verify(accessToken);
    } catch (e) {
      if (e.name === 'JsonWebTokenError') {
        throw new UnauthorizedException(
          'the provided token is not formated correctly.',
          'tokenNotValid',
        );
      } else if (e.name === 'TokenExpiredError') {
        throw new UnauthorizedException(
          'the provided token has been expired.',
          'tokenExpired',
        );
      } else {
        throw new UnauthorizedException(
          'unknown authorized exception.',
          'unknownException',
        );
      }
    }

    request.user = user;

    return true;
  }

  async validateUser(email, password): Promise<Auth> {
    const user = await this.userService.findByEmail(email);
    if (!user) {
      throw new UnauthorizedException(`email ${email} not found.`);
    }

    const newData = (user as any).toObject();

    let isValid = null;
    try {
      isValid = await bcrypt.compare(password, newData.password);
    } catch (e) {
      console.log(e);
    }

    if (!isValid) {
      throw new UnauthorizedException(
        `wrong password provided for email ${email}.`,
      );
    }
    return await this.doLogin(user);
  }

  async refreshTokens(accessToken: string, refreshToken: string) {
    const userPayload = await this.validateForRefresh(accessToken, true);
    const passwordPayload = await this.validateForRefresh(refreshToken);
    const user = await this.userService.findById(userPayload.sub);
    if (user.password !== passwordPayload.sub) {
      throw new UnauthorizedException(
        'the access token is not match with refresh token.',
      );
    }

    return this.doLogin(user);
  }
  private createAccessToken(user: any): string {
    const { password, __v, _id, ...info } = user.toObject();
    const payload = {
      sub: user.id,
      id: user.id,
      ...info,
    };

    return this.jwtService.sign(payload, {
      expiresIn: this.configService.accessTokenExpiry,
    });
  }

  private createRefreshToken(user: any): string {
    const payload = {
      sub: user.password,
    };

    return this.jwtService.sign(payload, {
      expiresIn: this.configService.refreshTokenExpiry,
    });
  }

  private async doLogin(user: any): Promise<Auth> {
    const accessToken = await this.createAccessToken(user);
    const refreshToken = await this.createRefreshToken(user);

    if (user.toObject) {
      user = { id: user.id, ...user.toObject() };
    }

    return {
      accessToken,
      refreshToken,
      user,
    };
  }

  private async validateForRefresh(
    token: string,
    allowExpiry = false,
  ): Promise<any> {
    let payload = null;
    try {
      payload = await this.jwtService.verify(token);
    } catch (e) {
      if (e.name === 'TokenExpiredError' && allowExpiry) {
        payload = await this.jwtService.decode(token);
      }
    }

    if (!payload) {
      throw new UnauthorizedException(
        'provided token is not valid for refresh.',
        'tokenNotValid',
      );
    }

    return payload;
  }

  private extractAccessToken(request: Request) {
    const authHeader = request.headers.authorization;
    if (!authHeader) {
      throw new UnauthorizedException(
        'authorization header not provided.',
        'tokenNotProvided',
      );
    }
    const headerParts = authHeader.split(' ');
    if (headerParts.length !== 2) {
      throw new UnauthorizedException(
        'the provided authorization header not valid bearer token.',
        'tokenNotValid',
      );
    }

    if (headerParts[0].toLowerCase() !== 'bearer') {
      throw new UnauthorizedException(
        'the provided authorization token should start with bearer word.',
        'tokenNotValid',
      );
    }

    return headerParts[1];
  }
}
