import { ArgsType, Field } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@ArgsType()
export class RefreshTokensDto {
  @Field()
  @IsNotEmpty()
  @ApiProperty()
  accessToken: string;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  refreshToken: string;
}
