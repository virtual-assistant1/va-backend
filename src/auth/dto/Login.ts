import { ArgsType, Field } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@ArgsType()
export class LoginDto {
  @Field()
  @IsEmail()
  @ApiProperty()
  email: string;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  password: string;
}
