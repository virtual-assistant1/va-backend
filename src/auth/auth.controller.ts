import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto, RefreshTokensDto } from './dto';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('Authentication APIs')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @ApiOperation({
    summary:
      'API used for Application Login',
  })
  login(@Body() body: LoginDto) {
    return this.authService.validateUser(body.email, body.password);
  }

  @Post('refreshTokens')
  @ApiOperation({
    summary:
      'API used fot refersh token',
  })
  refreshTokens(@Body() body: RefreshTokensDto) {
    return this.authService.refreshTokens(body.accessToken, body.refreshToken);
  }
}
