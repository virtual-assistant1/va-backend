import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@InputType()
export class CreateAppointmentDto {
  @Field()
  @IsNotEmpty()
  @ApiProperty()
  doctorId: string;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  date: Date;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  time: string;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  email: string;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  mobile: string;
}
