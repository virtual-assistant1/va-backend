import { prop } from '@typegoose/typegoose';
import { ObjectType, ID, Field } from '@nestjs/graphql';

@ObjectType()
export class Appointment {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  doctorId: string;

  @Field()
  @prop()
  userId: string;

  @Field()
  @prop()
  date: Date;

  @Field()
  @prop()
  time: string;

  @Field()
  @prop()
  email: string;

  @Field()
  @prop()
  mobile: string;

  @Field()
  @prop()
  name: string;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}
