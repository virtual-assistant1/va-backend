import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { AppointmentsResolver } from './appointments.resolver';
import { AppointmentsController } from './appointments.controller';
import { AppointmentsService } from './appointments.service';
import { Appointment } from './appointment.model';

@Module({
  imports: [
    TypegooseModule.forFeature([
      {
        typegooseClass: Appointment,
        schemaOptions: {
          timestamps: true,
        },
      },
    ]),
  ],
  providers: [AppointmentsResolver, AppointmentsService],
  controllers: [AppointmentsController],
})
export class AppointmentsModule {}
