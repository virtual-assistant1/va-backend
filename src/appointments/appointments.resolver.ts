import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/auth/auth.guard';
import { AppointmentsService } from './appointments.service';
import { Appointment } from './appointment.model';
import { CurrentUser } from 'src/auth/user.decorator';
import { CreateAppointmentDto } from './dto/createAppointment.dto';

@Resolver('Appointments')
export class AppointmentsResolver {
  constructor(private appointmentsService: AppointmentsService) {}

  @Query(() => [Appointment])
  @UseGuards(AuthGuard)
  appointments(@CurrentUser() user) {
    return this.appointmentsService.getUserAppointments(user.id);
  }

  @Mutation(() => Appointment)
  @UseGuards(AuthGuard)
  createAppointment(
    @CurrentUser() user,
    @Args('appointment') appointment: CreateAppointmentDto,
  ) {
    return this.appointmentsService.createAppointment(user.id, appointment);
  }
}
