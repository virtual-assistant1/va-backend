import { Controller, Get, Post, Body } from '@nestjs/common';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/auth/auth.guard';
import { AppointmentsService } from './appointments.service';
import { CurrentUser } from 'src/auth/user.decorator';
import { CreateAppointmentDto } from './dto/createAppointment.dto';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
@ApiBearerAuth()
@ApiTags('Appointments APIs')
@Controller('appointments')
export class AppointmentsController {
  constructor(private appointmentsService: AppointmentsService) {}

  @Get()
  @ApiOperation({
    summary: 'API to retrieve the user appointments',
  })
  @UseGuards(AuthGuard)
  userAppointments(@CurrentUser() user) {
    console.log(user);
    return this.appointmentsService.getUserAppointments(user.id);
  }

  @Post()
  @ApiOperation({
    summary: 'API to create an appointment',
  })
  @UseGuards(AuthGuard)
  createAppointment(
    @CurrentUser() user,
    @Body() appointment: CreateAppointmentDto,
  ) {
    return this.appointmentsService.createAppointment(user.id, appointment);
  }
}
