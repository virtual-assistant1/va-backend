import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Appointment } from './appointment.model';

@Injectable()
export class AppointmentsService {
  constructor(
    @InjectModel(Appointment)
    private readonly appointmentRepo: ReturnModelType<typeof Appointment>,
  ) {}

  createAppointment(userId, appointment) {
    return this.appointmentRepo.create({ ...appointment, userId });
  }

  getUserAppointments(userId) {
    return this.appointmentRepo.find({ userId });
  }
}
