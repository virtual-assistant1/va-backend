/* eslint-disable @typescript-eslint/camelcase */
import { Injectable, HttpService } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { ConfigService } from '../config/config.service';
import { Answer } from './answer.model';
import { Question } from './question.model';

@Injectable()
export class MlService {
  constructor(
    private httpService: HttpService,
    private config: ConfigService,
    @InjectModel(Question)
    private readonly questionRepo: ReturnModelType<typeof Question>,
    @InjectModel(Answer)
    private readonly answerRepo: ReturnModelType<typeof Answer>,
  ) {}

  async predict(args) {
    const results = await this.httpService
      .post(`${this.config.mlURL}/predict`, args)
      .toPromise();

    return results.data;
  }

  getQuestions(answer: number = null) {
    if (answer == null) {
      return this.questionRepo.find({ 'trigger_answer.0': { $exists: false } });
    } else {
      return this.questionRepo.find({ trigger_answer: answer });
    }
  }

  getAnswers(question: number) {
    return this.answerRepo.find({ question }).sort({ order: 1 });
  }

  getMainQuestionsCount(parentQuestion: number, questionType: string) {
    return this.questionRepo.find({ parentQuestion, questionType }).count();
  }
}
