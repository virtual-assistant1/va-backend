import { prop } from '@typegoose/typegoose';
import { ObjectType, ID, Field } from '@nestjs/graphql';
import { Answer } from './answer.model';

@ObjectType()
class Description {
  @Field()
  @prop()
  arabic: string;

  @Field()
  @prop()
  english: string;
}

@ObjectType()
export class Question {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  code: number;

  @Field({ description: 'the name of question' })
  @prop()
  question: string;

  @Field(() => Description)
  @prop()
  description: Description;

  @Field()
  @prop()
  parentQuestion: number;

  @Field()
  @prop()
  questionType: string;

  @Field()
  @prop()
  url: string;

  @prop()
  trigger_answer: number[];

  answers: Answer[];
}
