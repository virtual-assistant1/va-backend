import { Module } from '@nestjs/common';
import { MlService } from './ml.service';
import { EvaluationService } from './evaluation.service';
import { MlResolver } from './ml.resolver';
import { AnswersResolver } from './answers.resolver';
import { QuestionResolver } from './questions.resolver';
import { DiseaseResolver } from './disease.resolver';
import { EvaluationResolver } from './evaluation.resolver';
import { MlController } from './ml.controller';
import { TypegooseModule } from 'nestjs-typegoose';
import { Question } from './question.model';
import { Answer } from './answer.model';
import { Symptom } from './symptoms.model';
import { Disease } from './disease.model';
import { Prediction } from './prediction.model';

@Module({
  imports: [
    TypegooseModule.forFeature([
      {
        typegooseClass: Question,
        schemaOptions: {
          timestamps: true,
        },
      },
      {
        typegooseClass: Answer,
        schemaOptions: {
          timestamps: true,
        },
      },
      {
        typegooseClass: Symptom,
        schemaOptions: {
          timestamps: true,
        },
      },
      {
        typegooseClass: Disease,
        schemaOptions: {
          timestamps: true,
        },
      },

      {
        typegooseClass: Prediction,
        schemaOptions: {
          timestamps: true,
        },
      },
    ]),
  ],
  controllers: [MlController],
  providers: [
    MlService,
    EvaluationService,
    MlResolver,
    QuestionResolver,
    AnswersResolver,
    DiseaseResolver,
    EvaluationResolver,
  ],
})
export class MlModule {}
