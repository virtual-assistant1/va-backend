import { prop } from '@typegoose/typegoose';
import { ObjectType, ID, Field } from '@nestjs/graphql';

@ObjectType()
class SymptomQuestions {
  @Field()
  @prop()
  question: number;

  @Field(() => [Number])
  @prop()
  answers: number[];
}

@ObjectType()
export class Symptom {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  code: number;

  @Field()
  @prop()
  symptom: string;

  @Field(() => [SymptomQuestions])
  @prop()
  questions: SymptomQuestions[];
}
