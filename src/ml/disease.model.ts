import { prop } from '@typegoose/typegoose';
import { ObjectType, ID, Field } from '@nestjs/graphql';

@ObjectType()
export class Disease {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  code: number;

  @Field()
  @prop()
  disease: string;

  @Field(() => [Number])
  @prop()
  symptoms: number[];

  @Field()
  rank: number;
}
