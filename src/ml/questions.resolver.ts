import {
  Resolver,
  Query,
  Args,
  ResolveField,
  Parent,
  Int,
} from '@nestjs/graphql';
import { MlService } from './ml.service';
import { Answer } from './answer.model';
import { Question } from './question.model';
import { AuthGuard } from '../auth/auth.guard';
import { UseGuards } from '@nestjs/common';

@Resolver(() => Question)
export class QuestionResolver {
  constructor(private mlService: MlService) {}

  @ResolveField('answers', () => [Answer])
  async answers(@Parent() question: Question) {
    const results = await this.mlService.getAnswers(question.code);
    return results;
  }

  @Query(() => [Question])
  @UseGuards(AuthGuard)
  questions(@Args('answer') answer: number) {
    return this.mlService.getQuestions(answer);
  }

  @Query(() => [Question])
  @UseGuards(AuthGuard)
  firstQuestions() {
    return this.mlService.getQuestions();
  }

  @Query(() => Int)
  @UseGuards(AuthGuard)
  questionCount(
    @Args('parentQuestion') parentQuestion: number,
    @Args('questionType') questionType: string,
  ) {
    return this.mlService.getMainQuestionsCount(parentQuestion, questionType);
  }
}
