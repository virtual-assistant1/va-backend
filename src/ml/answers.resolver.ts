import { Resolver, Query, Args } from '@nestjs/graphql';
import { MlService } from './ml.service';
import { AuthGuard } from '../auth/auth.guard';
import { UseGuards } from '@nestjs/common';
import { Answer } from './answer.model';

@Resolver(() => Answer)
export class AnswersResolver {
  constructor(private mlService: MlService) {}

  @Query(() => [Answer])
  @UseGuards(AuthGuard)
  answers(@Args('question') question: number) {
    return this.mlService.getAnswers(question);
  }
}
