/* eslint-disable @typescript-eslint/camelcase */
import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Symptom } from './symptoms.model';
import { Disease } from './disease.model';
import { Answer } from './answer.model';
import { Question } from './question.model';
import { Prediction } from './prediction.model';
import { QADto } from './dto/evaluate.dto';
import { SaveDiseasesDto, SaveUserPropsDto } from './dto/saveEvaluation.dto';

@Injectable()
export class EvaluationService {
  constructor(
    @InjectModel(Symptom)
    private readonly symptomRepo: ReturnModelType<typeof Symptom>,
    @InjectModel(Disease)
    private readonly diseaseRepo: ReturnModelType<typeof Disease>,
    @InjectModel(Question)
    private readonly questionsRepo: ReturnModelType<typeof Question>,
    @InjectModel(Answer)
    private readonly answersRepo: ReturnModelType<typeof Answer>,
    @InjectModel(Prediction)
    private readonly predictionRepo: ReturnModelType<typeof Prediction>,
  ) {}

  async evaluate(questions: QADto[]) {
    const qIds = questions.map(i => i.question);
    const aIds = questions.flatMap(i => i.answers);

    let symptomsList = await this.symptomRepo
      .find({
        'questions.question': { $in: qIds },
        $expr: { $setIsSubset: ['$questions.question', qIds] },
      })
      .lean();

    symptomsList = symptomsList.filter(i =>
      i.questions.every(q => q.answers.some(a => aIds.indexOf(a) !== -1)),
    );

    const symptomsIds = symptomsList.map(i => i.code);

    let diseaseList = await this.diseaseRepo
      .find({
        symptoms: { $in: symptomsIds },
      })
      .lean();

    diseaseList = diseaseList.map(i => {
      let count = 0;
      i.symptoms.forEach(s => {
        if (symptomsIds.indexOf(s) !== -1) {
          count++;
        }
      });
      return { ...i, id: i._id, rank: count / i.symptoms.length };
    });

    return diseaseList.sort((a, b) => b.rank - a.rank).slice(0, 5);
  }

  async saveEvaluation(
    questions: QADto[],
    diseases: SaveDiseasesDto[],
    userProps: SaveUserPropsDto,
    risk: boolean,
  ) {
    const qIds = questions.map(i => i.question);
    const aIds = questions.flatMap(i => i.answers);

    const questionInfo = await this.questionsRepo
      .find({ code: { $in: qIds } })
      .lean();
    const answersInfo = await this.answersRepo
      .find({ code: { $in: aIds } })
      .lean();

    const questionData: Question[] = questions.map(i => {
      const q = questionInfo.find(q => i.question === q.code);
      return {
        id: q._id,
        code: q.code,
        question: q.question,
        trigger_answer: [],
        description: q.description,
        parentQuestion: q.parentQuestion,
        questionType: q.questionType,
        url: q.url,
        answers: i.answers.map(j => {
          const a = answersInfo.find(a => a.code === j);

          return {
            id: a._id,
            code: a.code,
            question: a.question,
            answer: a.answer,
          };
        }),
      };
    });

    const final = {
      user: userProps,
      questions: questionData,
      diseases,
      risk,
    };
    return this.predictionRepo.create(final);
  }

  getUserEvaluation(userId: string) {
    return this.predictionRepo
      .find({ 'user.userId': userId })
      .sort({ createdAt: -1 });
  }

  registerValidation(id: string, validationResult: boolean) {
    return this.predictionRepo.findByIdAndUpdate(
      id,
      {
        validationResult,
        validated: true,
      },
      {
        upsert: true,
        setDefaultsOnInsert: true,
        new: true,
      },
    );
  }
}
