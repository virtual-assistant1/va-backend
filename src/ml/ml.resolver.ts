import { Resolver, Query, Args } from '@nestjs/graphql';
import { PredictResults } from './dto/predict.results.dto';
import { PredictDto } from './dto/predict.dto';
import { MlService } from './ml.service';
import { AuthGuard } from '../auth/auth.guard';
import { UseGuards } from '@nestjs/common';
import { Answer } from './answer.model';

@Resolver(() => PredictResults)
export class MlResolver {
  constructor(private mlService: MlService) {}

  @Query(() => PredictResults)
  @UseGuards(AuthGuard)
  predict(@Args() args: PredictDto) {
    return this.mlService.predict(args);
  }

  @Query(() => [Answer])
  @UseGuards(AuthGuard)
  answers(@Args('question') question: number) {
    return this.mlService.getAnswers(question);
  }
}
