import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { AuthGuard } from '../auth/auth.guard';
import { UseGuards } from '@nestjs/common';
import { EvaluationService } from './evaluation.service';
import { Prediction } from './prediction.model';
import { SaveEvaluationDto } from './dto/saveEvaluation.dto';
import { CurrentUser } from '../auth/user.decorator';
import { RegisterValidation } from './dto/RegisterValidation.dto';

@Resolver(() => Prediction)
export class EvaluationResolver {
  constructor(private evaluationService: EvaluationService) {}

  @Mutation(() => Prediction)
  @UseGuards(AuthGuard)
  saveEvaluation(@Args() args: SaveEvaluationDto) {
    return this.evaluationService.saveEvaluation(
      args.questions,
      args.diseases,
      args.userProps,
      args.risk,
    );
  }

  @Query(() => [Prediction])
  @UseGuards(AuthGuard)
  userEvaluations(@CurrentUser() user) {
    return this.evaluationService.getUserEvaluation(user.id);
  }

  @Mutation(() => Prediction)
  @UseGuards(AuthGuard)
  registerValidation(@Args('validation') validation: RegisterValidation) {
    return this.evaluationService.registerValidation(
      validation.id,
      validation.validationResults,
    );
  }
}
