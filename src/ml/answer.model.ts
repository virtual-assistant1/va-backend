import { prop } from '@typegoose/typegoose';
import { ObjectType, ID, Field } from '@nestjs/graphql';

@ObjectType()
export class Answer {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  code: number;

  @Field()
  @prop()
  question: number;

  @Field()
  @prop()
  answer: string;

  @prop()
  order?: number;
}
