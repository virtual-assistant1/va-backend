import { prop, arrayProp } from '@typegoose/typegoose';
import { ObjectType, ID, Field } from '@nestjs/graphql';

@ObjectType()
class EvaluationAnswer {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  code: number;

  @Field()
  @prop()
  answer: string;
}

@ObjectType()
class EvaluationQuestion {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  code: number;

  @Field()
  @prop()
  question: string;

  @Field(() => [EvaluationAnswer])
  @arrayProp({ items: EvaluationAnswer })
  answers: EvaluationAnswer[];
}

@ObjectType()
class UserProps {
  @Field(() => String)
  @prop()
  userId: string;

  @Field()
  @prop()
  gender: string;

  @Field()
  @prop()
  age_year: number;

  @Field()
  @prop()
  weight: number;

  @Field()
  @prop()
  height: number;

  @Field()
  @prop()
  alcohol: number;

  @Field()
  @prop()
  smoke: number;

  @Field()
  @prop()
  glucose: number;

  @Field()
  @prop()
  active: number;

  @Field()
  @prop()
  cholesterol: number;

  @Field()
  @prop()
  systolic: number;

  @Field()
  @prop()
  diastolic: number;
}

@ObjectType()
export class EvaluationDisease {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  code: number;

  @Field()
  @prop()
  disease: string;

  @Field()
  @prop()
  rank: number;
}

@ObjectType()
export class Prediction {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  user: UserProps;

  @Field(() => [EvaluationQuestion])
  @arrayProp({ items: EvaluationQuestion })
  questions: EvaluationQuestion[];

  @Field(() => [EvaluationDisease])
  @arrayProp({ items: EvaluationDisease })
  diseases: EvaluationDisease[];

  @Field()
  @prop()
  risk: boolean;

  @Field({ nullable: true })
  @prop({ default: false })
  validated?: boolean = false;

  @Field({ nullable: true })
  @prop()
  validationResult?: boolean;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}
