import { ObjectType, Field } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

// error: boolean , message: string
@ObjectType()
export class PredictResults {
  @Field()
  @IsNotEmpty()
  error: boolean;

  @Field()
  @IsNotEmpty()
  message: string;
}
