import { InputType, Field, ArgsType } from '@nestjs/graphql';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { QADto } from './evaluate.dto';

// weight
// height
// gender
// systolic
// diastolic
// age_year
// cholesterol
// alcohol
// smoke
// glucose
// active

@InputType()
export class SaveDiseasesDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  @Field()
  code: number;

  @IsNotEmpty()
  @ApiProperty()
  @Field()
  disease: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  @Field()
  rank: number;
}

@InputType()
export class SaveUserPropsDto {
  @Field()
  @ApiProperty()
  @IsNotEmpty()
  userId: string;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  gender: string;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  age_year: number;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  weight: number;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  height: number;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  alcohol: number;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  smoke: number;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  glucose: number;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  active: number;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  cholesterol: number;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  systolic: number;

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  diastolic: number;
}

@ArgsType()
export class SaveEvaluationDto {
  @Field(() => [QADto])
  @ApiProperty()
  @IsNotEmpty()
  questions: QADto[];

  @Field(() => SaveUserPropsDto)
  @ApiProperty()
  @IsNotEmpty()
  userProps: SaveUserPropsDto;

  @Field(() => [SaveDiseasesDto])
  @ApiProperty()
  @IsNotEmpty()
  diseases: SaveDiseasesDto[];

  @Field()
  @ApiProperty()
  @IsNotEmpty()
  risk: boolean;
}
