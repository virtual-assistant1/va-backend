import { ArgsType, Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@InputType()
export class QADto {
  @Field()
  @IsNotEmpty()
  @ApiProperty()
  question: number;

  @Field(() => [Number])
  @IsNotEmpty()
  @ApiProperty()
  answers: number[];
}

// "systolic","diastolic","age_year","cholesterol","pulse"
@ArgsType()
export class EvaluateDto {
  @Field(() => [QADto])
  @ApiProperty()
  questions: QADto[];
}
