import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@InputType()
export class RegisterValidation {
  @Field()
  @IsNotEmpty()
  @ApiProperty()
  id: string;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  validationResults: boolean;
}
