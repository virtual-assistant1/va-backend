import { ArgsType, Field } from '@nestjs/graphql';
import { IsNotEmpty, IsInt, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

// weight
// height
// gender
// systolic
// diastolic
// age_year
// cholesterol
// alcohol
// smoke
// glucose
// active

@ArgsType()
export class PredictDto {
  @Field()
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  systolic: number;

  @Field()
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  diastolic: number;

  @Field()
  @IsNotEmpty()
  @IsInt()
  @ApiProperty()
  age_year: number;

  @Field()
  @IsNotEmpty()
  @IsInt()
  @ApiProperty()
  cholesterol: number;

  @Field()
  @IsNotEmpty()
  @IsInt()
  @ApiProperty()
  weight: number;

  @Field()
  @IsNotEmpty()
  @IsInt()
  @ApiProperty()
  height: number;

  @Field()
  @IsNotEmpty()
  @IsInt()
  @ApiProperty()
  gender: number;

  @Field()
  @IsNotEmpty()
  @IsInt()
  @ApiProperty()
  alcohol: number;

  @Field()
  @IsNotEmpty()
  @IsInt()
  @ApiProperty()
  smoke: number;

  @Field()
  @IsNotEmpty()
  @IsInt()
  @ApiProperty()
  glucose: number;

  @Field()
  @IsNotEmpty()
  @IsInt()
  @ApiProperty()
  active: number;
}
