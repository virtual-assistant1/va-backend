import { Resolver, Query, Args } from '@nestjs/graphql';
import { AuthGuard } from '../auth/auth.guard';
import { UseGuards } from '@nestjs/common';
import { EvaluationService } from './evaluation.service';
import { Disease } from './disease.model';
import { EvaluateDto } from './dto/evaluate.dto';

@Resolver(() => Disease)
export class DiseaseResolver {
  constructor(private evaluationService: EvaluationService) {}

  @Query(() => [Disease])
  @UseGuards(AuthGuard)
  evaluateAnswers(@Args() data: EvaluateDto) {
    return this.evaluationService.evaluate(data.questions);
  }
}
