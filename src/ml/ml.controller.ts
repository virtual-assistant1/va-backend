import { Controller, Body, Post, Get, Param } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { PredictDto } from './dto/predict.dto';
import { MlService } from './ml.service';
import { AuthGuard } from '../auth/auth.guard';
import { UseGuards } from '@nestjs/common';

@ApiBearerAuth()
@ApiTags('Machine Learning Prediction API and Diseases Symptoms APIs')
@Controller('ml')
export class MlController {
  constructor(private mlService: MlService) {}

  @Post('predict')
  @ApiOperation({
    summary:
      'Machine learning API used to predict if  patient is more or less likely to have cardiovascular disease , Machine Learning Prediction Module used in API Core development',
  })
  @UseGuards(AuthGuard)
  predict(@Body() args: PredictDto) {
    return this.mlService.predict(args);
  }

  @Get('questions/:answer')
  @ApiOperation({
    summary:
      'API used to get the next question according to answer of current question',
  })
  @UseGuards(AuthGuard)
  getQuestions(@Param('answer') answer: number) {
    return this.mlService.getQuestions(answer);
  }

  @Get('questions')
  @ApiOperation({
    summary: 'API used to retrieve the disease Symptom questions',
  })
  @UseGuards(AuthGuard)
  getFirstQuestions() {
    return this.mlService.getQuestions();
  }

  @Get('questions/:questionType/:parentQuestion')
  @UseGuards(AuthGuard)
  QuestionCount(
    @Param('parentQuestion') parentQuestion: number,
    @Param('questionType') questionType: string,
  ) {
    return this.mlService.getMainQuestionsCount(parentQuestion, questionType);
  }
}
