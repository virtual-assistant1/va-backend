import { Module, Global, HttpModule } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { ConfigModule } from '../config/config.module';

@Global()
@Module({
  imports: [AuthModule, ConfigModule, HttpModule],
  exports: [AuthModule, ConfigModule, HttpModule],
})
export class SharedModule {}
