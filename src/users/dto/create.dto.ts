import { IsEmail, IsNotEmpty } from 'class-validator';
import { InputType, Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';

@InputType()
export class CreateDto {
  @Field()
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  email: string;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  password: string;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @Field({ nullable: true })
  @ApiProperty()
  gender?: string;

  @Field({ nullable: true })
  @ApiProperty()
  dob?: Date;

  @Field({ nullable: true })
  @ApiProperty()
  weight?: number;

  @Field({ nullable: true })
  @ApiProperty()
  height?: number;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  userType: string;
}
