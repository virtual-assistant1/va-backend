import { InputType, Field } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';

@InputType()
export class UpdateDto {
  @Field({ nullable: true })
  @ApiProperty()
  name?: string;

  @Field({ nullable: true })
  @ApiProperty()
  gender?: string;

  @Field({ nullable: true })
  @ApiProperty()
  dob?: Date;

  @Field({ nullable: true })
  @ApiProperty()
  weight?: number;

  @Field({ nullable: true })
  @ApiProperty()
  height?: number;

  @Field({ nullable: true })
  @ApiProperty()
  userType?: string;
}
