import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { User } from './user.model';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User) private readonly repo: ReturnModelType<typeof User>,
  ) {}

  find() {
    return this.repo.find();
  }
  async findByEmail(email: string): Promise<User | undefined> {
    return await this.repo.findOne({ email });
  }
  create(args) {
    return this.repo.create(args);
  }

  async findById(id: string): Promise<User> {
    return await this.repo.findById(id);
  }

  async update(id: string, args: any) {
    return this.repo.findByIdAndUpdate(id, args, {
      upsert: true,
      setDefaultsOnInsert: true,
      new: true,
    });
  }
}
