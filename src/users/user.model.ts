import { prop, pre } from '@typegoose/typegoose';
import { ObjectType, ID, Field } from '@nestjs/graphql';

import * as bcrypt from 'bcrypt';

@ObjectType()
@pre<User>('save', async function(next) {
  if (!this.isModified('password')) {
    return next();
  }
  this.password = await bcrypt.hash(this.password, 10);
  return next();
})
export class User {
  @Field(() => ID)
  id: string;

  @Field()
  @prop({ validate: /\S+@\S+\.\S+/, unique: true })
  email: string;

  @prop({ required: true, minlength: 6 })
  password: string;

  @Field()
  @prop()
  name: string;

  @Field()
  @prop()
  gender: string;

  @Field()
  @prop()
  dob: Date;

  @Field()
  @prop()
  weight: number;

  @Field()
  @prop()
  height: number;

  @Field()
  @prop()
  userType: string;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}
