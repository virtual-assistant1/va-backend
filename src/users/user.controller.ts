import { Controller, Body, Put, Get, UseGuards, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateDto, UpdateDto } from './dto';
import { AuthGuard } from '../auth/auth.guard';
import { CurrentUser } from 'src/auth/user.decorator';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('Users Managment APIs')
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}
  @UseGuards(AuthGuard)
  @Get()
  @ApiOperation({
    summary:
      'API to retrive application users with their informations',
  })
  index() {
    return this.userService.find();
  }

  @Post()
  @ApiOperation({
    summary:
      'API used to create a new application user',
  })
  create(@Body() body: CreateDto) {
    return this.userService.create({
      email: body.email,
      password: body.password,
      name: body.name,
      gender: body.gender,
      dob: body.dob,
      height: body.height,
      weight: body.weight,
    });
  }

  @Put()
  @ApiOperation({
    summary:
      'API used to update application user profile',
  })
  update(@Body() body: UpdateDto, @CurrentUser() user) {
    return this.userService.update(user.id, body);
  }
}
