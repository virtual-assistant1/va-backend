import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { AuthGuard } from '../auth/auth.guard';
import { User } from './user.model';
import { CurrentUser } from '../auth/user.decorator';
import { CreateDto, UpdateDto } from './dto';

@Resolver(() => User)
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Query(() => [User])
  @UseGuards(AuthGuard)
  users() {
    return this.userService.find();
  }

  @Query(() => User)
  @UseGuards(AuthGuard)
  me(@CurrentUser() user) {
    return this.userService.findById(user.id);
  }

  @Mutation(() => User)
  createUser(@Args('user') args: CreateDto) {
    return this.userService.create({
      email: args.email,
      password: args.password,
      name: args.name,
      gender: args.gender,
      dob: args.dob,
      height: args.height,
      weight: args.weight,
      userType: args.userType,
    });
  }

  @Mutation(() => User)
  @UseGuards(AuthGuard)
  updateUser(@Args('user') args: UpdateDto, @CurrentUser() user) {
    return this.userService.update(user.id, args);
  }
}
