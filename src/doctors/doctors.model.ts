import { prop } from '@typegoose/typegoose';
import { ObjectType, ID, Field } from '@nestjs/graphql';

// code,specialization,name,address,telephone,mobile,city,experience,openingTime,closingTime
@ObjectType()
export class Doctor {
  @Field(() => ID)
  id: string;

  @Field()
  @prop()
  code: number;

  @Field()
  @prop()
  specialization: string;

  @Field()
  @prop()
  name: string;

  @Field()
  @prop()
  address: string;

  @Field()
  @prop()
  telephone: string;

  @Field()
  @prop()
  mobile: string;

  @Field()
  @prop()
  city: string;

  @Field()
  @prop()
  experience: string;

  @Field()
  @prop()
  openingTime: string;

  @Field()
  @prop()
  closingTime: string;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}
