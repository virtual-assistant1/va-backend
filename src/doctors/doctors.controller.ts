import { Controller, Get } from '@nestjs/common';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/auth/auth.guard';
import { DoctorsService } from './doctors.service';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('Doctors APIs')
@Controller('doctors')

export class DoctorsController {
  constructor(private doctorsService: DoctorsService) {}

  @Get()
  @ApiOperation({
    summary:
      'API to retrive the doctors with their informations',
  })
  @UseGuards(AuthGuard)
  doctors() {
    return this.doctorsService.getAllDoctors();
  }
}
