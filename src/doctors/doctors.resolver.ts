import { Resolver, Query } from '@nestjs/graphql';
import { Doctor } from './doctors.model';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/auth/auth.guard';
import { DoctorsService } from './doctors.service';

@Resolver(() => Doctor)
export class DoctorsResolver {
  constructor(private doctorsService: DoctorsService) {}

  @Query(() => [Doctor])
  @UseGuards(AuthGuard)
  doctors() {
    return this.doctorsService.getAllDoctors();
  }
}
