import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Doctor } from './doctors.model';

@Injectable()
export class DoctorsService {
  constructor(
    @InjectModel(Doctor)
    private readonly doctorRepo: ReturnModelType<typeof Doctor>,
  ) {}

  getAllDoctors() {
    return this.doctorRepo.find({});
  }
}
