import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { DoctorsController } from './doctors.controller';
import { DoctorsResolver } from './doctors.resolver';
import { DoctorsService } from './doctors.service';
import { Doctor } from './doctors.model';

@Module({
  imports: [
    TypegooseModule.forFeature([
      {
        typegooseClass: Doctor,
        schemaOptions: {
          timestamps: true,
        },
      },
    ]),
  ],
  controllers: [DoctorsController],
  providers: [DoctorsResolver, DoctorsService],
})
export class DoctorsModule {}
